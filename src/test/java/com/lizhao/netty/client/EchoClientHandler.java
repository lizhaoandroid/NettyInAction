package com.lizhao.netty.client;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.CharsetUtil;

public class EchoClientHandler extends SimpleChannelInboundHandler<ByteBuf> {

  @Override
  public void channelActive(ChannelHandlerContext ctx) throws Exception {
    //当与服务器连接成功时，调用改方法(往服务器发送信息)
    System.out.println("Client channelActive ");
    ctx.writeAndFlush(Unpooled.copiedBuffer(" hello,lizhao ", CharsetUtil.UTF_8));
    super.channelActive(ctx);
  }

  protected void channelRead0(ChannelHandlerContext ctx, ByteBuf msg) throws Exception {
    //服务器返回信息回来
    System.out.println("Client channelRead " + msg.toString(CharsetUtil.UTF_8));
  }

  @Override
  public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
    System.out.println("exceptionCaught ");
    cause.printStackTrace();
    ctx.close();
    super.exceptionCaught(ctx, cause);
  }
}
