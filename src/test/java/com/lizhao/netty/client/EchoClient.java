package com.lizhao.netty.client;


import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

public class EchoClient {
//  private final String host = "192.168.2.101";
//private final String host = "127.0.0.1";
  private final String host = "localhost";
  private final int port = 9080;

  public void start() throws Exception {
//    final EchoServerHandler echoServerHandler = new EchoServerHandler();
    EventLoopGroup eventLoopGroup = new NioEventLoopGroup();
    try {
      final Bootstrap serverBootstrap = new Bootstrap();
      serverBootstrap.group(eventLoopGroup).channel(NioSocketChannel.class)
//              .remoteAddress(new InetSocketAddress(host,port))
              .handler(new ChannelInitializer<SocketChannel>() {
                protected void initChannel(SocketChannel ch) throws Exception {
                  ch.pipeline().addLast(new EchoClientHandler());
//                  ChannelFuture channelFuture = serverBootstrap.bind().sync();
//                  channelFuture.channel().closeFuture().sync();
                }
              });
      ChannelFuture channelFuture = serverBootstrap.connect(host,port).sync();
      channelFuture.channel().closeFuture().sync();
    } finally {
      eventLoopGroup.shutdownGracefully().sync();
    }

  }

}
