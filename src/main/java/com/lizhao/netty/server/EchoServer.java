package com.lizhao.netty.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * 服务器
 *
 */
public class EchoServer {
  private final int port = 9080;

  public void start() throws Exception {
    final EchoServerHandler echoServerHandler = new EchoServerHandler();
    EventLoopGroup eventLoopGroup = new NioEventLoopGroup();
    try {
      final ServerBootstrap serverBootstrap = new ServerBootstrap();
      serverBootstrap.group(eventLoopGroup);
      serverBootstrap.channel(NioServerSocketChannel.class);//使用的是NIO传输，可以使用http方式
      serverBootstrap.localAddress(port);//绑定端口
      serverBootstrap.childHandler(new ChannelInitializer<SocketChannel>() {
                protected void initChannel(SocketChannel ch) throws Exception {
                  ch.pipeline().addLast(echoServerHandler);
                }
              });
      ChannelFuture channelFuture = serverBootstrap.bind().sync();//绑定服务器，调用sync表示阻塞等待该方法完成
      channelFuture.channel().closeFuture().sync();//获取channelFuture，调用sync表示阻塞等待该方法完成
    }finally {
      eventLoopGroup.shutdownGracefully().sync();
    }

  }
}
